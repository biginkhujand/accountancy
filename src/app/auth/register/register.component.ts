import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Title, Meta } from '@angular/platform-browser';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { UsersService } from '../../shared/services/users.service';
import { User } from '../../shared/models/user.model';
import { Message } from '../../shared/models/message.model';
import { Utils } from '../../shared/utils/utils';
import { fadeStateTrigger } from '../../shared/animations/fade.animation';

@Component({
    selector: 'wfm-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
    animations: [fadeStateTrigger]
})
export class RegisterComponent implements OnInit, OnDestroy {

    private unsubscribe: Subject<void> = new Subject<void>();

    form: FormGroup;
    message: Message;

    constructor(
        private usersService: UsersService,
        private router: Router,
        private title: Title,
        private meta: Meta) {
        title.setTitle('Регистрация');
        meta.addTags([
            { name: 'keywords', content: 'регистрация, система' },
            { name: 'description', content: 'Страница для регистрации' }
        ]);
    }

    ngOnInit() {
        this.form = new FormGroup({
            'email': new FormControl(null, [Validators.required, Validators.email]),
            'password': new FormControl(null, [Validators.required, Validators.minLength(6)]),
            'name': new FormControl(null, [Validators.required]),
            'agree': new FormControl(false, [Validators.requiredTrue])
        })
    }

    onSubmit() {
        const { email, password, name } = this.form.value;
        const user = new User(email, password, name);

        this.usersService.signUp(user)
            .takeUntil(this.unsubscribe)
            .subscribe((res) => {
                if (res.success) {
                    this.router.navigate(['/login'], {
                        queryParams: {
                            nowCanLogin: true
                        }
                    });
                } else {
                    this.message = Utils.showMessage(new Message(res.message.type, res.message.text));
                }
            });
    }

    ngOnDestroy() {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }
}
