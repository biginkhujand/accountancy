import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { UsersService } from '../../shared/services/users.service';
import { AuthService } from '../../shared/services/auth.service';
import { User } from '../../shared/models/user.model';
import { Message } from '../../shared/models/message.model';
import { Utils } from '../../shared/utils/utils';
import { fadeStateTrigger } from '../../shared/animations/fade.animation';

@Component({
    selector: 'wfm-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [fadeStateTrigger]

})
export class LoginComponent implements OnInit, OnDestroy {

    private unsubscribe: Subject<void> = new Subject<void>();

    form: FormGroup;
    message: Message;

    constructor(
        private usersService: UsersService,
        private authService: AuthService,
        private router: Router,
        private route: ActivatedRoute,
        private title: Title,
        private meta: Meta) {
        title.setTitle('Вход в систему');
        meta.addTags([
            { name: 'keywords', content: 'логин, вход, система' },
            { name: 'description', content: 'Страница для входа в систему' }
        ]);
    }

    ngOnInit() {
        this.route.queryParams
            .takeUntil(this.unsubscribe)
            .subscribe((params: Params) => {
                if (params['nowCanLogin']) {
                    this.message = Utils.showMessage(new Message('success', 'Теперь вы можете зайти в систему.'));
                } else if (params['accessDenied']) {
                    this.message = Utils.showMessage(new Message('danger', 'Для работы с системой вам необходимо войти.'));
                }
            })
        this.form = new FormGroup({
            'email': new FormControl(null, [Validators.required, Validators.email]),
            'password': new FormControl(null, [Validators.required, Validators.minLength(6)])
        })
    }

    onSubmit() {
        const { email, password } = this.form.value;
        const user = new User(email, password);

        this.usersService.signIn(user)
            .takeUntil(this.unsubscribe)
            .subscribe((res) => {
                if (res.success) {
                    this.authService.user = res;
                    this.authService.login();
                    this.router.navigate(['/system', 'bill']);
                } else {
                    this.message = Utils.showMessage(new Message(res.message.type, res.message.text));
                }
            })
    }

    ngOnDestroy() {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }
}