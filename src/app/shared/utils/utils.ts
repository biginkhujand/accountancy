import { Message } from '../models/message.model';

export class Utils {
    public static showMessage(message: Message) {
        setTimeout(() => {
            message.text = '';
        }, 5000);
        return message;
    }
}