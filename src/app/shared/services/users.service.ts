import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { User } from '../models/user.model';

@Injectable()
export class UsersService {

    private baseUrl = 'http://localhost:3000/';

    constructor(private http: HttpClient) {
    }

    signIn(user: User): Observable<any> {
        return this.http.post(this.baseUrl + 'signin', user);
    }

    signUp(user: User): Observable<any> {
        return this.http.post(this.baseUrl + 'signup', user);
    }
} 