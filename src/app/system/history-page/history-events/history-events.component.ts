import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { WFMEvent } from '../../shared/models/event.model';
import { Category } from '../../shared/models/category.model';
import { EventsService } from '../../shared/services/events.service';
import { CategoriesService } from '../../shared/services/categories.service';

@Component({
    selector: 'wfm-history-events',
    templateUrl: './history-events.component.html',
    styleUrls: ['./history-events.component.scss']
})
export class HistoryEventsComponent implements OnInit {

    isLoaded = false;
    searchValue = '';
    searchPlaceholder = 'Сумма';
    searchField = 'amount';   

    @Input() categories: Category[] = [];
    @Input() events: WFMEvent[] = [];

    constructor() { }

    ngOnInit() {
        this.events.forEach((event) => {
            event.catName = this.categories.find(category => category.id === event.category_id).name;
        });
    }

    changeCriteria(field: string) {
        const namesMap = {
            amount: 'Сумма',
            created_at: 'Дата',
            category: 'Категория',
            description: 'Описание',
            type: 'Тип'
        };

        this.searchPlaceholder = namesMap[field];
        this.searchField = field;
    }
}
