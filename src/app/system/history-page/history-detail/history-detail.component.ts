import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { EventsService } from '../../shared/services/events.service';
import { CategoriesService } from '../../shared/services/categories.service';
import { WFMEvent } from '../../shared/models/event.model';
import { Category } from '../../shared/models/category.model';
import { AuthService } from '../../../shared/services/auth.service';

@Component({
    selector: 'wfm-history-detail',
    templateUrl: './history-detail.component.html',
    styleUrls: ['./history-detail.component.scss']
})
export class HistoryDetailComponent implements OnInit, OnDestroy {

    private unsubscribe: Subject<void> = new Subject<void>();

    isLoaded = false;

    category: Category;
    event: WFMEvent;

    constructor(private eventsService: EventsService,
        private categoryService: CategoriesService,
        private authService: AuthService,
        private route: ActivatedRoute) { }

    ngOnInit() {
        this.route.params
            .mergeMap((params: Params) => this.eventsService.getEventById(this.authService.user.id, params['id']))
            .mergeMap((data: any) => {
                this.event = data.event;
                return this.categoryService.getCategoryById(this.authService.user.id, this.event.category_id);
            })
            .takeUntil(this.unsubscribe)
            .subscribe((data: any) => {
                this.category = data.category;

                this.isLoaded = true;
            });
    }

    ngOnDestroy() {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

}
