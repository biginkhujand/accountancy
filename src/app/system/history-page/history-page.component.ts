import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';
import * as moment from 'moment';

import { CategoriesService } from '../shared/services/categories.service';
import { EventsService } from '../shared/services/events.service';
import { AuthService } from '../../shared/services/auth.service';
import { Observable } from 'rxjs/Observable';
import { Category } from '../shared/models/category.model';
import { WFMEvent } from '../shared/models/event.model';
import { Message } from '../../shared/models/message.model';
import { Utils } from '../../shared/utils/utils';

@Component({
    selector: 'wfm-history-page',
    templateUrl: './history-page.component.html',
    styleUrls: ['./history-page.component.scss']
})
export class HistoryPageComponent implements OnInit, OnDestroy {

    private unsubscribe: Subject<void> = new Subject<void>();

    isLoaded = false;
    isFilterVisible = false;
    
    categories: Category[] = [];
    chartData = [];
    events: WFMEvent[] = [];
    filteredEvents: WFMEvent[] = [];
    message: Message;

    constructor(private categoriesService: CategoriesService,
        private eventsService: EventsService,
        private authService: AuthService) { }

    ngOnInit() {
        Observable.combineLatest(
            this.categoriesService.getCategories(this.authService.user.id),
            this.eventsService.getEvents(this.authService.user.id))
            .takeUntil(this.unsubscribe)
            .subscribe((data: any) => {
                this.categories = data[0].categories;
                this.events = data[1].events;
                if (!data[1].success) {
                    this.message = Utils.showMessage(new Message(data[1].message.type, data[1].message.text));
                }
                this.setOriginalEvents();
                this.calculateChartData();

                this.isLoaded = true;
            });
    }

    private setOriginalEvents() {
        if (this.events) {
            this.filteredEvents = this.events.slice();
        }
    }

    calculateChartData(): void {
        this.chartData = [];

        if (this.categories) {
            this.categories.forEach((category) => {
                const catEvents = this.filteredEvents.filter(e => e.category_id === category.id && e.type === 'outcome');
                this.chartData.push({
                    name: category.name,
                    value: catEvents.reduce((total, e) => {
                        total += e.amount;
                        return total;
                    }, 0)
                });
            });
        }
    }

    private toggleFilterVisibility() {
        this.isFilterVisible = !this.isFilterVisible;;
    }

    openFilter() {
        this.toggleFilterVisibility();
    }

    onFilterApply(filterData) {
        this.toggleFilterVisibility();
        this.setOriginalEvents();

        const startPeriod = moment().startOf(filterData.period).startOf('d');
        const endPeriod = moment().endOf(filterData.period).endOf('d');

        this.filteredEvents = this.filteredEvents
            .filter((e) => {
                return filterData.types.indexOf(e.type) !== -1;
            })
            .filter((e) => {
                return filterData.categories.indexOf(e.category_id.toString()) !== -1;
            })
            .filter((e) => {
                const momemtDate = moment(e.created_at, 'YYYY-MM-DD HH:mm:ss');
                return momemtDate.isBetween(startPeriod, endPeriod);
            });

        this.calculateChartData();
    }

    onRefresh() {
        this.setOriginalEvents();
        this.calculateChartData();
    }

    onFilterCancel() {
        this.toggleFilterVisibility();
        this.setOriginalEvents();
        this.calculateChartData();
    }

    ngOnDestroy() {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }
}
