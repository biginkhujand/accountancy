import { Component, Output, EventEmitter, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { CategoriesService } from '../../shared/services/categories.service';
import { AuthService } from '../../../shared/services/auth.service';
import { Category } from '../../shared/models/category.model';
import { Message } from '../../../shared/models/message.model';
import { Utils } from '../../../shared/utils/utils';

@Component({
    selector: 'wfm-add-category',
    templateUrl: './add-category.component.html',
    styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnDestroy {

    private unsubscribe: Subject<void> = new Subject<void>();

    @Output() onCategoryAdd = new EventEmitter<Category>();

    message: Message;

    constructor(private categoryService: CategoriesService,
        private authService: AuthService) { }

    onSubmit(form: NgForm) {
        let { name, capacity } = form.value;
        if (capacity < 0) capacity *= -1;

        const category = new Category(name, capacity);

        this.categoryService.addCategory(this.authService.user.id, category)
            .takeUntil(this.unsubscribe)
            .subscribe((data: any) => {
                form.reset();
                form.form.patchValue({ capacity: 1 });
                this.onCategoryAdd.emit(data);
                this.message = Utils.showMessage(new Message(data.message.type, data.message.text));
            });
    }

    ngOnDestroy() {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

}
