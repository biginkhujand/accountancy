import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { Category } from '../../shared/models/category.model';
import { CategoriesService } from '../../shared/services/categories.service';
import { AuthService } from '../../../shared/services/auth.service';
import { Message } from '../../../shared/models/message.model';
import { Utils } from '../../../shared/utils/utils';

@Component({
    selector: 'wfm-edit-category',
    templateUrl: './edit-category.component.html',
    styleUrls: ['./edit-category.component.scss']
})
export class EditCategoryComponent implements OnInit, OnDestroy {

    private unsubscribe: Subject<void> = new Subject<void>();

    @Input() categories: Category[] = [];
    @Output() onCategoryEdit = new EventEmitter<Category>();

    currentCategoryId = 1;
    currentCategory: Category;
    message: Message;

    constructor(private categoryService: CategoriesService,
        private authService: AuthService) { }

    ngOnInit() {
        this.onCategoryChange();
    }

    onCategoryChange() {
        if (this.categories.length > 0) {
            this.currentCategory = this.categories
                .find(cat => cat.id === +this.currentCategoryId);
        }
    }

    onSubmit(form: NgForm) {
        let { name, capacity } = form.value;
        if (capacity < 0) capacity *= -1;

        const category = new Category(name, capacity, +this.currentCategoryId);

        this.categoryService.updateCategory(this.authService.user.id, category)
            .takeUntil(this.unsubscribe)
            .subscribe((data: any) => {
                this.onCategoryEdit.emit(data);
                this.message = Utils.showMessage(new Message(data.message.type, data.message.text));
            });
    }

    ngOnDestroy() {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }
}
