import { Component, OnInit, OnDestroy } from '@angular/core';
import { Category } from '../shared/models/category.model';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { CategoriesService } from '../shared/services/categories.service';
import { AuthService } from '../../shared/services/auth.service';

@Component({
    selector: 'wfm-records-page',
    templateUrl: './records-page.component.html',
    styleUrls: ['./records-page.component.scss']
})
export class RecordsPageComponent implements OnInit, OnDestroy {

    private unsubscribe: Subject<void> = new Subject<void>();

    isLoaded = false;

    categories: Category[] = [];

    constructor(private serviceCategory: CategoriesService,
        private authService: AuthService) { }

    ngOnInit() {
        this.getCategories();
    }

    getCategories() {
        this.serviceCategory.getCategories(this.authService.user.id)
            .takeUntil(this.unsubscribe)
            .subscribe((data: any) => {
                if (data.success) {
                    this.categories = data.categories;
                } else {
                    this.categories = [];
                }

                this.isLoaded = true;
            });
    }

    newCategoryAdded(category: Category) {
        this.categories.push(category);
        this.getCategories();
    }

    categoryWasEdited(data: any) {
        this.getCategories();
        // const idx = this.categories
        //     .findIndex(cat => cat.id === data.category.id);
        // this.categories[idx] = data.category;

    }

    ngOnDestroy() {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }
}
