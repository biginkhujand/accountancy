import { Component, OnDestroy, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { EventsService } from '../../shared/services/events.service';
import { BillServise } from '../../shared/services/bill.service';
import { AuthService } from '../../../shared/services/auth.service';
import { Category } from '../../shared/models/category.model';
import { WFMEvent } from '../../shared/models/event.model';
import { Message } from '../../../shared/models/message.model';
import { Utils } from '../../../shared/utils/utils';

@Component({
    selector: 'wfm-add-event',
    templateUrl: './add-event.component.html',
    styleUrls: ['./add-event.component.scss']
})
export class AddEventComponent implements OnDestroy {

    private unsubscribe: Subject<void> = new Subject<void>();

    @Input() categories: Category[] = [];

    types = [
        { type: 'income', label: 'Доход' },
        { type: 'outcome', label: 'Расход' }
    ]

    message: Message;

    constructor(private eventsService: EventsService,
        private billService: BillServise,
        private authService: AuthService) { }

    onSubmit(form: NgForm) {
        let { type, amount, description, category_id } = form.value;
        if (amount < 0) amount *= -1;

        const event = new WFMEvent(
            type, amount, description, this.authService.user.id, category_id
        );

        this.billService.getBill(this.authService.user.id)
            .takeUntil(this.unsubscribe)
            .subscribe((data: any) => {
                let value = 0;
                let bill = data.bill;
                if (type === 'outcome') {
                    if (amount > bill) {
                        this.message = Utils.showMessage(new Message('danger', `На счету недостаточно средств. Вам не хватает ${amount - bill}.`));
                        return;
                    } else {
                        value = bill - amount;
                        this.message = Utils.showMessage(new Message('success', `На счету осталось ${value}.`));                        
                    }
                } else {
                    value = bill + amount;
                    this.message = Utils.showMessage(new Message('success', `Прибавиление ${amount}, теперь на счету  ${value}.`));                                            
                }
                this.billService.updateBill(this.authService.user.id, value)
                    .mergeMap(() => this.eventsService.addEvent(this.authService.user.id, event))
                    .takeUntil(this.unsubscribe)
                    .subscribe((data: any) => {
                        form.setValue({
                            type: 'outcome',
                            amount: 1,
                            description: '',
                            category_id: 1,
                        });
                        form.controls.description.markAsUntouched();
                        if(!data.success) {
                            this.message = Utils.showMessage(new Message(data.message.type, data.message.text));                    
                        }
                    });
            });
    }

    ngOnDestroy() {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }
}
