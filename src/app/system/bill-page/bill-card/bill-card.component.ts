import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'wfm-bill-card',
  templateUrl: './bill-card.component.html',
  styleUrls: ['./bill-card.component.scss']
})
export class BillCardComponent implements OnInit {

  @Input() bill: number;
  @Input() currency: any;


  dollar: number;
  euro: number;

  constructor() { }

  ngOnInit() {
    const { rates } = this.currency;

    this.dollar = rates['USD'] * this.bill;
    this.euro = rates['EUR'] * this.bill;
  }

}
