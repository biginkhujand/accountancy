import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { BillServise } from '../shared/services/bill.service';
import { AuthService } from '../../shared/services/auth.service';

@Component({
    selector: 'wfm-bill-page',
    templateUrl: './bill-page.component.html',
    styleUrls: ['./bill-page.component.scss']
})
export class BillPageComponent implements OnInit, OnDestroy {

    private unsubscribe: Subject<void> = new Subject<void>();

    isLoaded = false;

    bill: number;
    currency: any;

    constructor(
        private billService: BillServise,
        private authService: AuthService) { }

    ngOnInit() {
        Observable.combineLatest(
            this.billService.getBill(this.authService.user.id),
            this.billService.getCurrency())
            .takeUntil(this.unsubscribe)
            .subscribe((data: any) => {
                this.bill = data[0].bill;
                this.currency = data[1];

                this.isLoaded = true;
            });
    }

    onRefresh() {
        this.isLoaded = false;
        this.billService.getCurrency()
            .takeUntil(this.unsubscribe)
            .subscribe((currency: any) => {
                this.currency = currency;

                this.isLoaded = true;
            })
    }

    ngOnDestroy() {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

}
