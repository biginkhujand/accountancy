import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { BillServise } from '../shared/services/bill.service';
import { EventsService } from '../shared/services/events.service';
import { CategoriesService } from '../shared/services/categories.service';
import { AuthService } from '../../shared/services/auth.service';
import { Category } from '../shared/models/category.model';
import { WFMEvent } from '../shared/models/event.model';
import { Message } from '../../shared/models/message.model';
import { Utils } from '../../shared/utils/utils';

@Component({
    selector: 'wfm-planning-page',
    templateUrl: './planning-page.component.html',
    styleUrls: ['./planning-page.component.scss']
})
export class PlanningPageComponent implements OnInit, OnDestroy {

    private unsubscribe: Subject<void> = new Subject<void>();

    isLoaded = false;
    
    bill: number;
    categories: Category[] = [];
    events: WFMEvent[] = [];
    message: Message;

    constructor(private billService: BillServise,
        private categoryService: CategoriesService,
        private eventsService: EventsService,
        private authService: AuthService) { }

    ngOnInit() {
        Observable.combineLatest(
            this.billService.getBill(this.authService.user.id),
            this.categoryService.getCategories(this.authService.user.id),
            this.eventsService.getEvents(this.authService.user.id))
            .takeUntil(this.unsubscribe)
            .subscribe((data: any) => {
                this.bill = data[0].bill;
                this.categories = data[1].categories;
                this.events = data[2].events;
                if (!data[1].success) {
                    this.message = Utils.showMessage(new Message(data[1].message.type, data[1].message.text));
                } else if (!data[2].success) {
                    this.message = Utils.showMessage(new Message(data[2].message.type, data[2].message.text));                    
                }
                
                this.isLoaded = true;
            });
    }

    getCategoryCost(category: Category): number {
        const catEvents = this.events.filter(e => e.category_id === category.id && e.type === 'outcome');
        return catEvents.reduce((total, e) => {
            total += e.amount;
            return total;
        }, 0);
    }

    private getPercent(category: Category): number {
        const percent = (100 * this.getCategoryCost(category)) / category.capacity;
        return percent > 100 ? 100 : percent;
    }

    getCategoryPercent(category: Category): string {
        return this.getPercent(category) + '%';
    }

    getCategoryColorClass(category: Category): string {
        const percent = this.getPercent(category);
        return percent < 60 ? 'success' : percent >= 100 ? 'danger' : 'warning';
    }

    ngOnDestroy() {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

}
