import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Category } from '../models/category.model';

@Injectable()
export class CategoriesService {

    private baseUrl = 'http://localhost:3000/';

    constructor(public http: HttpClient) { }

    addCategory(id: number, category: Category): Observable<any> {
        return this.http.post(this.baseUrl + `users/${id}/categories`, category);
    }

    getCategories(id: number): Observable<any> {
        return this.http.get(this.baseUrl + `users/${id}/categories`);
    }

    getCategoryById(id: number, catId: number): Observable<any> {
        return this.http.get(this.baseUrl + `users/${id}/categories/${catId}`);
    }

    updateCategory(id: number, category: Category): Observable<any> {
        return this.http.put(this.baseUrl + `users/${id}/categories/${category.id}`, category);
    }
}