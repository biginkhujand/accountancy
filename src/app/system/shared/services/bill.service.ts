import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class BillServise {

    private baseUrl = 'http://localhost:3000/';

    constructor(public http: HttpClient) { }

    getBill(id: number): Observable<any> {
        return this.http.get(this.baseUrl + 'users/' + id + '/bill');
    }

    updateBill(id: number, bill: number): Observable<any> {
        return this.http.put(this.baseUrl + `users/${id}/bill`, { bill: bill });
    }

    getCurrency(base: string = 'RUB'): Observable<any> {
        return this.http.get(`http://api.fixer.io/latest?base=${base}`);
    }
}