import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { WFMEvent } from '../models/event.model';

@Injectable()
export class EventsService {

    private baseUrl = 'http://localhost:3000/';

    constructor(public http: HttpClient) {
    }

    addEvent(id: number, event: WFMEvent): Observable<any> {
        return this.http.post(this.baseUrl + `users/${id}/events`, event);
    }

    getEvents(id: number): Observable<any> {
        return this.http.get(this.baseUrl + `users/${id}/events`);
    }

    getEventById(id: number, eventId: number): Observable<any> {
        return this.http.get(this.baseUrl + `users/${id}/events/${eventId}`);
    }
}