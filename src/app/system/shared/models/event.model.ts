export class WFMEvent {
    constructor(
        public type: string,
        public amount: number,
        public description: string,
        public user_id: number,
        public category_id: number,
        public created_at?: Date,
        public id?: number,
        public catName?: string
    ) { }
}